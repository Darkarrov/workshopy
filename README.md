# Web development workshopy ve skills-fighters

Informace ohledne planovanych workshopu ve Skills-FIghters.

# Naučíme se
    - Základy tvorby webových stránek a aplikací v javascript, html5 a css3
    - Statická webová stránka za použití standardních metod a stylů
    - kompilování a využití lokálního serveru pomocí NodeJs
    - Moderní javascript knihovna VueJs, která se aktuálně používá v mnoha firmách
    - Posílání a stahování dat ze serveru, funkcionalita "zdi" z facebooku, kam může každý přidávat data

# Co je potřeba mít na počítači
    - internetový prohlížeč
    - poznámkový blok
    - node.js (https://nodejs.org/en/)
    - visual studio code (https://code.visualstudio.com/)
    - git (https://git-scm.com/)
    - účet na gitlabu (https://gitlab.com/)

# Obsah hodin
1. ZÁKLADY HTML A CSS

popis:
    - vytvoření webové stránky pomocí codepen.io a notepadu

obsah:
    - html template
    - css styly
    - importy
    - základní javascript

výsledek:
    - Funkční statická webová stránka s více stránkami a použitím stylů a scriptu

materiály:
    - https://codepen.io/
    - https://stackoverflow.com/
    - https://www.youtube.com/watch?v=53zkBvL4ZB4
    - CZ: https://www.youtube.com/watch?v=vMicd97Z7CY
    - CZ: https://www.itnetwork.cz/html-css/html-manual


2. ZÁKLADY JAVASCRIPTU

popis:
    - programování v konzoli

obsah:
	- proměnné
	- if, for, while
	- funkce
	- pole, funkcionální metody pole
	- objekty

materiály:
    - https://www.youtube.com/watch?v=_y9oxzTGERs
    - CZ: https://www.itnetwork.cz/javascript/zaklady


2. PROJECT SETUP

popis:
    - V tomto díle konečně začneme pracovat s Vue a jeho nástavbou Nuxt a dalšími prostředky moderního webového developmentu. Naučíme se s transpilery - pug a scss. Naučíme se základy Vue componentů. Naúčíme se stáhnout package a použít ho v projektu, naučíme se stahovat data ze serveru a zobrazit je klientovi.

obsah:
    - create-nuxt-app
    - package, package.json
    - hot realoding, lokální server
    - eslint, server side rendering
    - transpilery - pug & scss
    - Vue komponent (template, script, styl)
    - axios

výsledek:
    - Nahrát data na server pomocí post requestu.

materiály: 
    - https://www.npmjs.com/
    - https://github.com/axios/axios
    - https://nuxtjs.org/
    - https://www.vzhurudolu.cz/amp/prirucka/rozcestnik-npm-node


3. VUE & NUXT

obsah:
    - Hooky
    - Direktivy
    - Componenty
    - Layouty
    - COnfig

výsledek:
    - Vypsat data stažená ze serveru.

materiály: 
    - https://www.youtube.com/watch?v=vMicd97Z7CY
    - https://www.youtube.com/watch?v=z6hQqgvGI4Y
    - https://vuejs.org/v2/guide/


4. BOOTSTRAP A DESIGN

obsah:
    - Bootstrap - col, row
    - flex, grid, float
    - Tooltip
    - Modal
    - <transform>
    - Bootstrap-vue, konponenty

výsledek:
    - mít celou stránku nastylovanou

materiály:
    - https://www.youtube.com/watch?v=Vu5QKn24uYs
    - CZ: https://www.itnetwork.cz/html-css/bootstrap/uvod-do-css-frameworku-bootstrap
    - https://bootstrap-vue.js.org/
    - http://getbootstrap.com/


5. GIT A FTP DEPLOYMENT, HOSTING

výsledek:
    - Vytvořit účet na gitlabu, udělat merge branche, deploynout aplikaci na hosting

materiály:
    - https://www.youtube.com/watch?v=Y9XZQO1n_7c


6. BEZPEČNOST a UX

obsah:
    - povídání o bezpečnosti ve webové aplikaci a základech UX
    - XSS scripty
    - CSS keylogery, javascript keylogery
    - Integrity
    - npm packages bezpečnost
    - UX
    - Trackery, reklamy, blockery, fingerprinting

materialy:
    - https://www.youtube.com/watch?v=L5l9lSnNMxg
    - https://www.youtube.com/watch?v=oJ6t7AImTdE
    - https://www.ublock.org/
    - https://www.youtube.com/watch?v=Ya1eTbTUUvc
    - https://www.youtube.com/watch?v=eQFbG6CwwdI
    - https://www.youtube.com/watch?v=0dgmeTy7X3I
    UX: https://www.youtube.com/watch?v=8vfbVVkwdQw