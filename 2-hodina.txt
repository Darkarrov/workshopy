Diskuze:
Brave webovy prohlizec - https://brave.com/

Prubeh:
Prace s konzoli weboveho prohlizece
Klicove slovo let
Klicove slovo var
Klicove slovo const
Popis objektoveho pristupu a vytvoreni objektu s promennyma a funkcemi
Podminky IF/ELSE
Kontrola hodnot: == a ===
Cyklus FOR
Cyklus WHILE
Pole
Funkce filter a map nad polem
Funkce alert a prompt
Funkce uvnitr pole a vyvolani funkce z venku
Cteni rest API

Vystup console:
SW registered
val karel = 10
VM129:1 Uncaught SyntaxError: Unexpected identifier
let karel = 10
undefined
karel + 4
14
karel = 20
20
karel + 4
24
karel - 10
10
karel / 3
6.666666666666667
Math.log(10)
2.302585092994046
karel % 5
0
karel % 2
0
karel % 3
2
{ let karel = 55, console.log('karel', karel) }
VM394:1 Uncaught SyntaxError: Unexpected token .
const jozef = 22
undefined
jozef
22
jozef = 33
VM450:1 Uncaught TypeError: Assignment to constant variable.
    at <anonymous>:1:7
(anonymous) @ VM450:1
karel = { vek = 33, vaha = 56 }
VM487:1 Uncaught SyntaxError: Invalid shorthand property initializer
karel = ( vek = 33, vaha = 56 )
56
karel.vaha
undefined
karel = ( vek : 33, vaha : 56 )
VM516:1 Uncaught SyntaxError: Unexpected token :
karel = { vek : 33, vaha : 56 }
{vek: 33, vaha: 56}
karel.vek
33
karel = { vek : 33, vaha : 56, pribbrat : function() { this.vaha = this.vaha + 10 } }
{vek: 33, vaha: 56, pribbrat: ƒ}
karel.vaha
56
karel.pribbrat
ƒ () { this.vaha = this.vaha + 10 }
karel.vaha
56
karel.pribbrat()
undefined
karel.vaha
66
karel = { vek : 33, vaha : 56, pribbrat : function() { vaha = vaha + 10 } }
{vek: 33, vaha: 56, pribbrat: ƒ}
karel.vaha
56
karel.pribbrat()
undefined
karel.vaha
56
karel = { vek : 33, vaha : 56, pribbrat : function() { vaha = vaha + 10 } }
{vek: 33, vaha: 56, pribbrat: ƒ}
karel.vaha
56
karel.pribbrat()
undefined
karel.vaha
56
karel = { vek : 33, vaha : 56, pribbrat : function() { this.vaha = this.vaha + 10 } }
{vek: 33, vaha: 56, pribbrat: ƒ}
karel.vaha
56
karel.pribbrat()
undefined
karel.vaha
66
let adam = 10
undefined
adam
10
adam = 'ja jsem adam'
"ja jsem adam"
adam
"ja jsem adam"
if (adam == 'hovno') { confirm.log('je tam hovno') }
undefined
adam = 'hovno'
"hovno"
if (adam == 'hovno') { confirm.log('je tam hovno') }
VM881:1 Uncaught TypeError: confirm.log is not a function
    at <anonymous>:1:32
(anonymous) @ VM881:1